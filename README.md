# vbNET SendData #

Exemplo de projeto ecrito em vb.NET que comunica com os webservices da NappSolutions

### Requisitos ###

* NET Framework 4.5+
* Newtonsoft.Json

### Poss�veis Erros ###

* Ao compilar o projeto poder� ocorrer o erro: "VB.Net 'Sub Main' was not found", para arrumar v� ate "Project > [Your project] Properties > Application Tab"
* E troque o Startup Object de 'Module1' para 'Sub Main'.

### Contato ###

* Guilherme Zenatte - guilherme@nappsolutions.com
	
* Leandro Vieira - leandro@nappsolutions.com
	
* Caio Arthur - caio@nappsolutions.com

### WebSite ###

* www.nappsolutions.com
