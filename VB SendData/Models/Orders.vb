﻿Imports System.Text
Imports Newtonsoft.Json

Namespace Models
    Public Class Order
        Private OrderId As Integer
        Private OrderCode As [String]
        Private OrderTotal As Decimal
        Private OrderDate As DateTime
        Private OrderDoc As String
        Public Property coo() As String
            Get
                Return m_coo
            End Get
            Set
                m_coo = Value
            End Set
        End Property
        Private m_coo As String

        Public Property pedidoId() As Integer
            Get
                Return Me.OrderId
            End Get
            Set
                OrderId = Value
            End Set
        End Property

        Public Property pedidoCode() As [String]
            Get
                Return Me.OrderCode
            End Get
            Set
                OrderCode = Value
            End Set
        End Property

        Public Property valorTotal() As Decimal
            Get
                Return Me.OrderTotal
            End Get
            Set
                OrderTotal = Value
            End Set
        End Property

        Public ReadOnly Property dataPedido() As Long
            Get
                Dim epoch = New DateTime(1970, 1, 1, 0, 0, 0,
                    DateTimeKind.Utc)
                Return Convert.ToInt64((Me.OrderDate.ToUniversalTime() - epoch).TotalMilliseconds)
            End Get
        End Property

        Public Sub setDateTime(dt As DateTime)
            Me.OrderDate = dt
        End Sub

        Public Property documento() As String
            Get
                Return Me.OrderDoc
            End Get
            Set
                If Value.Trim().Length >= 11 Then
                    OrderDoc = Value.Trim()
                End If
            End Set
        End Property

        Public Overrides Function ToString() As String
            Dim str As New StringBuilder()
            Dim nLine As [String] = Environment.NewLine

            str.Append("Id: " + pedidoId + nLine)
            str.Append("Code: " + pedidoCode + nLine)
            str.Append("Total: " + valorTotal + nLine)
            str.Append("Date: " + dataPedido + nLine)
            str.Append((Convert.ToString("Documento: ") & documento) + nLine)

            Return str.ToString()
        End Function

        Public Function ToJson() As String
            Return JsonConvert.SerializeObject(Me)
        End Function
    End Class
End Namespace