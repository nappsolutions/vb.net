﻿Imports VB_SendData.Models
Imports VB_SendData.Utils

Namespace SendData
    Module Module1
        Public Sub Main(args As String())
            Dim orders As New List(Of Order)()

            'Venda 1
            Dim order1 As New Order()
            order1.pedidoCode = "vbTest1"
            order1.valorTotal = Convert.ToDecimal("80,50")
            order1.setDateTime(Convert.ToDateTime("17/08/2017"))
            orders.Add(order1)

            'Venda 2
            Dim order2 As New Order()
            order2.pedidoCode = "vbTest2"
            order2.valorTotal = Convert.ToDecimal("60,50")
            order2.setDateTime(Convert.ToDateTime("18/08/2017"))
            orders.Add(order2)

            'Venda 3
            Dim order3 As New Order()
            order3.pedidoCode = "vbTest3"
            order3.valorTotal = Convert.ToDecimal("73,50")
            order3.setDateTime(Convert.ToDateTime("19/08/2017"))
            orders.Add(order3)

            'Devolução 1
            Dim order4 As New Order()
            order4.pedidoCode = "vbTest4"
            order4.valorTotal = Convert.ToDecimal("-75,50")
            order4.setDateTime(Convert.ToDateTime("19/08/2017"))
            orders.Add(order4)

            Dim lojaId As Integer = 1
            Dim lojaNome As String = "Nome da Loja"
            Dim usuario As String = "Usuario Loja"
            Dim senha As String = "Senha Loja"
            Dim ambiente As Boolean = False
            ' True = Produção - False = Teste
            Dim rede As String = "Shopping Rede"

            Dim send As New Send(orders, lojaId, lojaNome, usuario, senha, ambiente,
                rede)
            send.send()
        End Sub
    End Module
End Namespace