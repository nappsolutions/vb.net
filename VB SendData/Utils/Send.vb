﻿Imports VB_SendData.Models
Imports Newtonsoft.Json
Imports System.IO
Imports System.Net
Imports System.Text

Namespace Utils
    Class Send
        Private orders As List(Of Order)
        Private sResponse As ServerResponse
        Private sds As SendDataStore

        Private storeId As Integer = 0
        Private storeName As String = ""
        Private username As String = ""
        Private password As String = ""
        Private environment As Boolean
        Private mall As String

        Public Sub New(orders As List(Of Order), storeId As Integer, storeName As String, username As String, password As String, environment As Boolean,
            mall As String)
            ' Corrige um erro que raramente acontece
            ' O cliente inclui um header Expect: 100-Continue
            ' E o servidor responde com 417 - Expectation Failed
            System.Net.ServicePointManager.Expect100Continue = False

            ' Set Orders
            Me.orders = orders
            Me.storeId = storeId
            Me.storeName = storeName
            Me.username = username
            Me.password = password
            Me.environment = environment
            Me.mall = mall

            ' Object json
            sds = New SendDataStore(Me.storeId, Me.storeName, orders)
        End Sub

        Public Sub send()
            Console.WriteLine([String].Format("Enviando {0} registro(s) de {1}", sds.getCurrentPosition() + sds.getIndex(), Me.orders.Count))

            ' Convert orders to string
            Dim contentOrders As [String] = JsonConvert.SerializeObject(sds.getData())

            ' Get URI based environment
            Dim URI As [String] = ""

            If Me.environment Then
                URI = [String].Format("https://{0}.nappsolutions.com", mall)
            Else
                URI = [String].Format("http://{0}.sandbox.nappsolutions.com", mall)
            End If

            URI += "/service/receiver"

            ' POST data
            Dim postData = "content=" + contentOrders
            Dim data = Encoding.ASCII.GetBytes(postData)

            ' Build request
            Dim request As HttpWebRequest = DirectCast(WebRequest.Create(URI), HttpWebRequest)
            request.Method = "POST"
            request.ContentLength = data.Length
            request.ContentType = "application/x-www-form-urlencoded"

            ' Request credentials
            request.Credentials = New NetworkCredential(Me.username, Me.password)

            ' Write data to request
            Dim dataStream As Stream = request.GetRequestStream()
            dataStream.Write(data, 0, data.Length)
            dataStream.Close()

            ' Do Request
            Dim response As HttpWebResponse = DirectCast(request.GetResponse(), HttpWebResponse)
            Dim rString As [String] = New StreamReader(response.GetResponseStream()).ReadToEnd()

            ' Parse response
            sResponse = JsonConvert.DeserializeObject(Of ServerResponse)(rString)
            sResponse.countProcessed(sResponse.total)

            ' Show server response
            Console.WriteLine(sResponse)

            If Not sResponse.status Then
                Throw New Exception("O servidor respondeu com um erro: " + sResponse.msg.Trim())
            End If

            If sds.hasData() Then
                Me.send()
            End If
        End Sub

        Public Function getResponse() As ServerResponse
            Return sResponse
        End Function
    End Class

    Class SendDataStore
        Public idLoja As Integer
        Public nomeLoja As [String]
        Private pedidosTuple As List(Of Order)
        Public pedidos As List(Of Order)
        Private index As Integer

        Public Sub New(id As Integer, name As [String], orders As List(Of Order))
            Me.idLoja = id
            Me.nomeLoja = name
            Me.pedidosTuple = orders
            Me.index = 0
        End Sub

        Public Function getData() As SendDataStore
            If Not hasData() Then
                Return Me
            End If

            Dim position As Integer = getCurrentPosition()
            Me.pedidos = pedidosTuple.GetRange(Me.index, position)
            Me.index += position
            Return Me
        End Function

        Public Function getIndex() As Integer
            Return index
        End Function

        Public Function getCurrentPosition() As Integer
            Dim total As Integer = 1000
            If index + total > pedidosTuple.Count - 1 Then
                total = pedidosTuple.Count - index
            End If

            Return total
        End Function

        Public Function hasData() As Boolean
            If pedidosTuple.Count >= index + 1 Then
                Return True
            End If

            Return False
        End Function
    End Class

    Class ServerResponse
        Public status As Boolean
        Public msg As [String]
        Public total As UInt32
        Private Shared sum As UInteger = 0

        Public Overrides Function ToString() As String
            Dim output As [String] = [String].Format("{0}, {1} registro(s) novo(s)", If((status), "Ok", "Fail"), total)

            Return output
        End Function

        Public Sub countProcessed(total As UInteger)
            sum += total
        End Sub

        Public Function getProcessed() As UInteger
            Return sum
        End Function
    End Class
End Namespace